---
title: "Creating YouTube openers with generative design"
date: 2020-03-03T22:44:35-05:00
draft: false
coverimage: "https://storage.googleapis.com/iamhamy-static/archive/projects/2020/hamy-yt-logo-maudern/hamy-logo-maudern-hamy-colors-screenshot.png"
tags: []
showonlyimage: false
---

Over the past few weeks I've been rethinking my brand and identity as a maker. One of the biggest challenges was coming up with what it was that I wanted to make - for me, everything else falls into place from there.

What I decided on was something along the lines of marrying tech and art in interesting ways to produce an impact in the world. Perhaps not the most refined vision statement, but it's what I've got for now.

Since I'd just started a [YouTube channel](https://www.youtube.com/channel/UCPBY44jxP7gOMkUsP5rlGnw), I thought it would be a good place to start showing this direction more aggressively. Given that I'd done many [visual projects](https://labs.iamhamy.xyz/projects) in the past and that creative coding projects are what I want to focus on into the foreseeable future, I decided to make my own.

Because generative design is one of the core components of my projects to date, I wanted to find a way to display its properties - the beauty and the chaos. To this end, I retrofitted one of the projects I was working on, [maudern](https://labs.iamhamy.xyz/projects/maudern), to display my logo over its generation.

I then selected 3 color schemes that both pleased me and that I felt closely tied with - my HAMY color scheme of black and red evoking bold pragmatism, the Nike color scheme of black and green as I admire the minimalism and contrast, and the Instagram gradient as I enjoy the platform (and - disclaimer - currently work there) and like its playfulness as a foil to my two other schemes. Together, they create a suite of intros that can be cycled through to introduce my style of work while showcasing the basic tenants of generative design.

Here are those three intros:

HAMY - input YT videos of logo mauderns

If you want to learn more about the base project maudern, visit its [project page](https://labs.iamhamy.xyz/projects/maudern).

Want to collab? [Connect with me](https://iamhamy.xyz/connect) and we can set something up!