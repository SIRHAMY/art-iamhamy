// on document load
// add prettify and linenumber classes to all code nodes
// add prettify to page

window.onload = () => {
    let codeBlocks = document.getElementsByTagName("code");

    if(codeBlocks.length <= 0) {
        return;
    }
    for(const codeBlock of codeBlocks) {
        if(codeBlock.parentNode.nodeName !== "PRE") {
            continue;
        }
        codeBlock.classList.add("prettyprint");
        codeBlock.classList.add("linenums");
    }
    let newScriptNode = document.createElement("script");
    newScriptNode.setAttribute("type","text/javascript");
    newScriptNode.setAttribute("src", "https://cdn.jsdelivr.net/gh/google/code-prettify@master/loader/run_prettify.js?skin=sunburst");
    let headNode = document.getElementsByTagName("head")[0];
    headNode.appendChild(newScriptNode);
};