FROM sirhamy/hugo-base as hugobase

EXPOSE 80

RUN mkdir /usr/share/hugo-build
COPY site/ /usr/share/hugo-build
WORKDIR /usr/share/hugo-build
RUN hugo

FROM nginx:alpine
COPY nginx.conf /etc/nginx/nginx.conf
COPY --from=hugobase /usr/share/hugo-build/public /usr/share/nginx/html